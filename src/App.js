import './App.css';
import React from 'react';
import { Contador } from './Contador';
import { Buscador } from './Buscador';
import { Listas } from './Listas';
import { Item } from './Item';
import { Boton } from './Boton';

function App() {
  //Contador
  //Estado

  const localStoragePers = localStorage.getItem('TAREAS_v1');
  let personas;

  if (!localStoragePers) {
    localStorage.setItem('TAREAS_v1', JSON.stringify([]));
    personas = [];
  } else {
    personas = JSON.parse(localStoragePers);
  }

  //Contador
  //Estado
  const [tareas, setTareas] = React.useState(personas);

  //CONTADOR
  const [buscandoValor, setBuscandoValor] = React.useState('');

  //Varibles que almacena las tareas completadas
  const compledtTareas = tareas.filter((tarea) => !!tarea.completed).length;
  const totalTareas = tareas.length;

  let buscarTareas = [];


  //Condicional
  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tareas) => {
      const tareaText = tareas.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  //Funcion para actualizar localStorage
  const saveTareas = (newTareas) => {
    const stringTareas = JSON.stringify(newTareas);
    localStorage.setItem('TAREAS_v1', stringTareas);
    setTareas(newTareas);
  }

  const addTarea = (text) => {
    const newTareas = [...tareas];
    newTareas.push({
      completed: false,
      text,
    });
    saveTareas(newTareas);
  };

  const completarTarea = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const newTareas = [...tareas];
    newTareas[tareaIndex].completed = true;
    saveTareas(newTareas);
  };

  const deleteTarea = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const newTareas = [...tareas];
    newTareas.splice(tareaIndex, 1);
    saveTareas(newTareas);
  };

  return (
    <React.Fragment>
      <Contador total={totalTareas} completed={compledtTareas} />
      <Boton
        addTarea={addTarea}
      />
      <Buscador
        buscandoValor={buscandoValor}
        setBuscandoValor={setBuscandoValor}
      />
      <Listas>
        {buscarTareas.map((tarea) => (
          <Item
            text={tarea.text}
            key={tarea.text}
            completed={tarea.completed}
            onComplete={() => completarTarea(tarea.text)}
            onDelete={() => deleteTarea(tarea.text)}
          />
        ))}
      </Listas>
    </React.Fragment>
  );
}


export default App;
