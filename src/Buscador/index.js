import React from 'react';
import './Buscador.css';

function Buscador({buscandoValor, setBuscandoValor}) {
//Estado
const buscando = (event) => {
  setBuscandoValor(event.target.value)
}

  return (
    <form>
      <div className='count-buscador'>
        <input type="text" id="buscador" name="buscandoValor" onChange={buscando}></input>
        <span class="highlight"></span>
        <span class="bar"></span>
        <label>Busca tu nota</label>
      </div>
    </form>
  );
}

export { Buscador };