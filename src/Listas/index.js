import React from 'react';
import './Listas.css';

function Listas(props) {
  return (
    <div>
        <section>
            <ul>
                {props.children}
            </ul>
        </section>
    </div>
  );
}

export { Listas };