import React from 'react';
import './Item.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTrash } from '@fortawesome/free-solid-svg-icons';

function Item(props) {
  return (
    <li className='item'>
      <p className='notas'>{props.text}</p>
      <div className='check'>
        <button 
        type="button" 
        className={`button-check ${props.completed && 'button-check--active'}`}
        onClick={props.onComplete}
        >
          <FontAwesomeIcon className='iconCheck' icon={faCheck} />
        </button>
      </div>
      <div className='trash'>
        <button 
          type="button" 
          className='button-trash'
          onClick={props.onDelete}
          >
          <FontAwesomeIcon className='iconTrash' icon={faTrash} />
        </button>
      </div>
    </li>
  );
}
export { Item };