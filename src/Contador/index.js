import React from 'react';
import './Contador.css';

function Contador({total, completed}) {
  return (
    <div className='contador'>
        <h1>Tareas realizadas: {completed} de {total}</h1>
    </div>
  );
}

export { Contador };