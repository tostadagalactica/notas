import React from 'react';
import './Boton.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Modal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

function Boton(props) {

  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = '#06296b';
  }

  function closeModal() {
    setIsOpen(false);
  }

  const [newTareaValue, setNewTareValue] = React.useState('');

  const onChange = (event) => {
    setNewTareValue(event.target.value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    props.addTarea(newTareaValue);
  };

  return (
    <div className="pos-nose">
      <div class="boton">
        <button
          type="button"
          className='button-plus'
          onClick={openModal}
        >
          <FontAwesomeIcon className='iconCheck' icon={faPlus} />
        </button>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Modal"
      >
        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Agrega una nueva nota</h2>
        <input
          className='input'
          value={newTareaValue}
          onChange={onChange}
        />
        <form onSubmit={onSubmit}>
          <button onClick={closeModal}>Cerrar</button>
          <button type="submit">Añadir</button>
        </form>
      </Modal>
    </div>
  );
}

export { Boton };