//Configuración del sw

const ESTATICO_CACHE = 'static-v2';
const DINAMICO_CACHE = 'dinamico-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL = [
  '/',
  'index.html',
  'manifest.json',
  'js/app.js',
  'js/sw-utils.js',
  'Notas192.png',
  'Notas512.png',
];

const APP_SHELL_INMUTABLE = [];

//Proceso de instalación
self.addEventListener('install', (event) => {
  const cacheStatic = caches
    .open(ESTATICO_CACHE)
    .then((cache) => cache.addAll(APP_SHELL));
  const cacheInmutable = caches
    .open(INMUTABLE_CACHE)
    .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

  event.waitUntil(Promise.all[(cacheStatic, cacheInmutable)]);
});

//Proceso de activacion
self.addEventListener('activate', (event) => {
  //Eliminar el cache del SW anterior
  const respuesta = caches.keys().then((keys) => {
    keys.forEach((keys) => {
      if (keys !== ESTATICO_CACHE && keys.includes('static')) {
        return caches.delete(keys);
      }
    });
  });

  event.waitUntil(respuesta);
});

//Estrategia de cache
self.addEventListener('fetch', (event) => {
  const respuesta = caches.match(event.request).then((res) => {
    if (res) {
      return res;
    } else {
      return fetch(event.request).then((newResp) => {
        //priemro ir al archivp sw-utils.js
        return actualizaCacheDinamico(DINAMICO_CACHE, event.request, newResp);
      });
    }
  });
  event.respondWith(respuesta);
});

//Guardar en el cache dinamico
function actualizaCacheDinamico(cacheDinamico, req, res) {
  if (res.ok) {
    return caches.open(cacheDinamico).then((cache) => {
      cache.put(req, res.clone());
      return res.clone();
    });
  } else {
    return res;
  }
}
