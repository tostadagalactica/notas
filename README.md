# Notas

App para guardar y administrar las tareas por realizar.
## Install

```npm
npm install
```

# Usage

```bash
npm start
```

# Contriguting

Proyecto creado para la clase de GPS

# License

NotasApp is released under the [MIT Lincense] (https://opensource.org/licenses/MIT).